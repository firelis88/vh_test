<?php

/**
 * This is the model class for table "tb_source_rel".
 *
 * The followings are the available columns in table 'tb_source_rel':
 * @property string $cx
 * @property string $rx
 * @property string $title
 * @property string $ndc
 * @property string $subtitle
 */
class SourceRel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_source_rel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cx, rx', 'length', 'max'=>10),
			array('title', 'length', 'max'=>100),
			array('ndc', 'length', 'max'=>20),
			array('subtitle', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cx, rx, title, ndc, subtitle', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cx' => 'Cx',
			'rx' => 'Rx',
			'title' => 'Title',
			'ndc' => 'Ndc',
			'subtitle' => 'Subtitle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cx',$this->cx,true);
		$criteria->compare('rx',$this->rx,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('ndc',$this->ndc,true);
		$criteria->compare('subtitle',$this->subtitle,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SourceRel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
