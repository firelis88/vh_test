<?php

class m151102_145309_init_table__tb_source_rel extends CDbMigration
{
	public function up()
	{
		$query = '
			INSERT INTO `tb_source_rel`
			  (`cx`, `rx`, `title`, `ndc`, `subtitle`)
			(SELECT
			  tb_source.cx, tb_source.rx, tb_source.title, tb_rel.ndc, SUBSTR(tb_source.title, 1, 7)
			FROM
			  tb_source
			LEFT JOIN tb_rel
			ON tb_source.cx = tb_rel.cx)
			UNION
			(SELECT
			  tb_source.cx, tb_source.rx, tb_source.title, tb_rel.ndc, SUBSTR(tb_source.title, 1, 7)
			FROM
			  tb_source
			RIGHT JOIN tb_rel
			ON tb_source.cx = tb_rel.cx
			)
		';

		$this->execute($query);

	}

	public function down()
	{

		$this->truncateTable('tb_source_rel');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}