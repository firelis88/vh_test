<?php

class m151102_145249_create_index__cx__on_table__tb_source extends CDbMigration
{
	public function up()
	{
		$this->createIndex('idx_cx', 'tb_source', 'cx');
	}

	public function down()
	{
		$this->dropIndex('idx_cx', 'tb_source');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}