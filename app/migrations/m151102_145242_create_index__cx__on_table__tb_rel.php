<?php

class m151102_145242_create_index__cx__on_table__tb_rel extends CDbMigration
{
	public function up()
	{
		$this->createIndex('idx_cx', 'tb_rel', 'cx');
	}

	public function down()
	{
		$this->dropIndex('idx_cx', 'tb_rel');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}