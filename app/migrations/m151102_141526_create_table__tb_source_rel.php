<?php

class m151102_141526_create_table__tb_source_rel extends CDbMigration
{
	public function up()
	{
		$query = '
		CREATE TABLE `tb_source_rel` (
  `cx` varchar(10) DEFAULT NULL,
  `rx` varchar(10) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `ndc` varchar(20) DEFAULT NULL,
  `subtitle` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
		';

		$this->execute($query);
	}

	public function down()
	{
		$this->dropTable('tb_source_rel');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}