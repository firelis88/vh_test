<?php

/**
 * @var SiteController $this
 * @var CSqlDataProvider $sqlDataProvider
 */

?>

<h2>Task 2</h2>
<p></p>
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
   'id' => 'source-grid',
    'dataProvider' => $sqlDataProvider,
    'type' => 'striped bordered hover',
    'enableSorting' => false,
    'columns' => array(
        array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'name' => 'cx'
        ),
        array(
            'name' => 'rx'
        ),
        array(
            'name' => 'title'
        ),
        array(
            'name' => 'ndc'
        ),
    )

));
?>