<?php
$this->breadcrumbs = array(
    'Ошибка ' . $code
);

$this->pageTitle = 'Ошибка ' . $code;
?>


<div class="page-header">
    <H1>Произошла ошибка</H1>
</div>
<p class="lead">
    <?= \CHtml::encode($message); ?>
</p>


