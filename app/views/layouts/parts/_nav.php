<?php $this->widget(
    'bootstrap.widgets.TbNavbar',
    array(
        'brandLabel' => Yii::app()->name,
        'fluid' => false,
        'display' => TbHtml::NAVBAR_DISPLAY_FIXEDTOP,
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbNav',
                'items' => array(
                    array('label' => 'Task 1', 'url' => Yii::app()->createUrl('/site/task1'), 'active' => Yii::app()->controller->action->id == 'task1'),
                    array('label' => 'Task 2', 'url' => Yii::app()->createUrl('/site/task2'), 'active' => Yii::app()->controller->action->id == 'task2'),
                ),
            ),
            array(
                'class' => 'bootstrap.widgets.TbNav',
                'htmlOptions' => array('class' => 'pull-right'),
                'items' => array(
                ),
            ),
        ),
    )
);