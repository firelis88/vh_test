<meta charset="utf-8">
<title><?= CHtml::encode($this->pageTitle) ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?
/** @var ClientScript $clientScript */
$clientScript = Yii::app()->getClientScript();

$baseUrl = Yii::app()->getBaseUrl();
$baseUrlA = Yii::app()->getBaseUrl(true);


$clientScript->registerCoreScript('jquery');
Yii::app()->bootstrap->register();

$clientScript->registerCSSFile($baseUrl . '/css/project.css');
?>
