<?php

/**
 * для установки локальных настроек, переименуйте в connect.php , и дополните настойки
 */

return array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=testdrive',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ),
    ),
);


