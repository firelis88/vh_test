<?php

$config = CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(//some vars
    )
);

if (file_exists(__DIR__ . '/connect.php')) {
    $config = CMap::mergeArray(
        $config,
        include __DIR__ . '/connect.php'
    );
}

if (file_exists(__DIR__ . '/params.php')) {
    $config = CMap::mergeArray(
        $config,
        include __DIR__ . '/params.php'
    );
}
return $config;