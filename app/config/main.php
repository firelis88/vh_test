<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
date_default_timezone_set('Europe/Moscow');
Yii::setPathOfAlias('vendor', __DIR__ . '/../../vendor');
Yii::setPathOfAlias('app', __DIR__ . '/..');
$config = array(
    'basePath' => __DIR__ . DIRECTORY_SEPARATOR . '..',
    'name' => 'VH Test Task',
    'sourceLanguage' => 'en',
    'language' => 'ru',
    // preloading 'log' component
    'preload' => array('log'),
    'modules' => array(),
    'aliases' => array(
        'bootstrap' => realpath(__DIR__ . '/../../vendor/2amigos/yiistrap'), // change this if necessary
        'yiiwheels' => realpath(__DIR__ . '/../../vendor/2amigos/yiiwheels'), // change this if necessary
    ),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'bootstrap.helpers.TbHtml',
    ),
    // application components
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
        ),
        'session' => array(
            'autoStart' => true,
        ),
        'cache' => array(
            'class' => 'CFileCache',
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',
        ),
        'yiiwheels' => array(
            'class' => 'yiiwheels.YiiWheels',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '' => 'site/task1',
                'site/' => 'site/task1',
                'site/index' => 'site/task1',
                'site/<action:\w+>' => 'site/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ),
        'log' => array( //configure log
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CProfileLogRoute',
                    'enabled' => YII_DEBUG && isset($_REQUEST['debug']),
                    'levels' => 'trace, info, profile, warning, error',
                    'categories' => array('system.*', 'ar.*'),
                ),
                array(
                    'class' => 'CEmailLogRoute',
                    'enabled' => !YII_DEBUG,
                    'levels' => 'error, warning',
                    'except' => 'exception.CHttpException.*',
                    // add your email in this section
                    'emails' => array(),
                ),
            ),
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),

    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'db_cache_duration' => 604800,
        'page_cache_duration' => 100,
    ),
);


return $config;
