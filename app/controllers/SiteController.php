<?php

class SiteController extends Controller {

    public function filters() {
        return array(
            array(
                'COutputCache',
                'duration' => Yii::app()->params['page_cache_duration'],
                'varyByRoute' => true,
                'varyByParam' => array('SourceRel_page', 'page'),
            ),
        );
    }

    public function actionTask1() {
        $criteria = new \CDbCriteria();
        $criteria->addCondition('t.subtitle = \'title 1\'');

        $activeDataProvider = new CActiveDataProvider(
            SourceRel::model()->cache(Yii::app()->params['db_cache_duration']),
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 1000
                )
            )
        );

        $this->render('task1', array('activeDataProvider' => $activeDataProvider));
    }

    public function actionTask2() {

        $query = Yii::app()->getDb()->cache(Yii::app()->params['db_cache_duration'])->createCommand()
            ->select('(@id := @id + 1) AS id, cx, rx, title, ndc')
            ->from('tb_source_rel')
            ->join('(SELECT @id := 0) tb', '1=1')
            ->where('subtitle = \'title 1\'')
            ->getText();

        $count = Yii::app()->getDb()->cache(Yii::app()->params['db_cache_duration'])->createCommand()
            ->select('COUNT(cx)')
            ->from('tb_source_rel')
            ->where('subtitle = \'title 1\'')
            ->queryScalar();

        $sqlDataProvider = new CSqlDataProvider(
            $query,
            array(
                'totalItemCount' => $count,
                'pagination' => array(
                    'pageSize' => 1000,
                )
            )
        );


        $this->render('task2', array('sqlDataProvider' => $sqlDataProvider));


    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

}
