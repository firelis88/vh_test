<?php

// change the following paths if necessary
$yiiBase = __DIR__ . '/../vendor/yiisoft/yii/framework/YiiBase.php';
$yii = __DIR__ . '/../app/yii.php';


$env = getenv('APP_ENV') ? getenv('APP_ENV') : 'dev';
$config = __DIR__ . '/../app/config/' . $env . '.php';


if ($env == 'dev') // remove the following lines when in production mode
{
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
}
require_once __DIR__ . '/../vendor/autoload.php';
require_once($yiiBase);
require_once($yii);
require_once __DIR__ . '/../app/app/WebApplication.php';
Yii::createWebApplication($config)->run();