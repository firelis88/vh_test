<?php
$projectRoot = __DIR__ . DIRECTORY_SEPARATOR;

$directoryList = array(
    array($projectRoot . 'app/runtime', 0777),
    array($projectRoot . 'www/assets', 0777),
);

foreach ($directoryList as $directory) {
    $chmod = 0777;
    if (is_array($directory)) {
        $chmod = $directory[1];
        $directory = $directory[0];
    }

    if (!file_exists($directory)) {
        mkdir($directory);
        echo 'Created directory ' . $directory . ' ' . PHP_EOL;
        chmod($directory, $chmod);
    }
}