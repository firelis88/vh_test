# Задачи 1 - 4

 Задачи и ответы в Task_1-4.html

# Задачи 5 - 7 

## Как развернуть проект

### Composer

```
> php composer.phar install
```


### Права на папки

```
> php checkRequirements.php
```

По умолчанию 0777 на www/assets, app/runtime.
 

### Конфиги
 
```
> cp app/config/connect.sample.php app/config/connect.php
```
Дополнить настройки.  


### Миграции
  
```
> app/yiic migrate
```


### Настройка .htaccess

Для разработки:

```
> cp www/.htaccess.sample-dev www/.htaccess
```

Проект готов к работе!

